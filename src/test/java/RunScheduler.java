import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.gritsay.SchedulerImpl;
import ru.gritsay.SchedulerQueueHandler;
import ru.gritsay.Task;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.PriorityBlockingQueue;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;

public class RunScheduler {

    private final static Logger log = LoggerFactory.getLogger(SchedulerQueueHandler.class);
    private SchedulerImpl scheduler;
    private PriorityBlockingQueue<Task> taskQueue = new PriorityBlockingQueue<>();

    @Before
    public void setUp() {
        scheduler = new SchedulerImpl(taskQueue);
    }

    @Test
    public void testScheduleAddJobs() throws InterruptedException {
        Set<Integer> income = new HashSet<>();
        Set<Integer> outcome = new HashSet<>();
        for (int i = 0; i < 1000; i++) {
            int pos = (int) (Math.random() * (i % 2 == 0 ? 10 : -10));
            LocalDateTime time = LocalDateTime.now().plusNanos(pos);
            income.add(pos);
            Callable<Object> actions = () -> {
                outcome.add(pos);
                return pos;
            };
            scheduler.schedule(time, actions);
        }
        sleep(30000);

        new Thread(() -> {
            for (int i = 0; i < 20; i++) {
                int pos = (int) (Math.random() * (i % 2 == 0 ? 10 : -10));
                LocalDateTime time = LocalDateTime.now().plusNanos(pos);
                income.add(pos);
                Callable<Object> actions = () -> {
                    outcome.add(pos);
                    return pos;
                };
                scheduler.schedule(time, actions);
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 20; i++) {
                int pos = (int) (Math.random() * (i % 2 == 0 ? 10 : -10));
                LocalDateTime time = LocalDateTime.now().plusNanos(pos);
                income.add(pos);
                Callable<Object> actions = () -> {
                    outcome.add(pos);
                    return pos;
                };
                scheduler.schedule(time, actions);
            }
        }).start();


        sleep(20000);
        assertEquals(income, outcome);
    }
}
