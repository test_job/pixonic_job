package ru.gritsay.interfaces;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

public interface Scheduler {
    void schedule(LocalDateTime runTime, Callable<Object> callable);
}
