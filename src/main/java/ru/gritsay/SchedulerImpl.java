package ru.gritsay;

import ru.gritsay.interfaces.Scheduler;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.PriorityBlockingQueue;

public class SchedulerImpl implements Scheduler {

    private final SchedulerQueueHandler queueHandler;
    private PriorityBlockingQueue<Task> taskQueue;

    public SchedulerImpl(PriorityBlockingQueue<Task> taskQueue) {
        this.taskQueue = taskQueue;
        queueHandler = new SchedulerQueueHandler(taskQueue);
    }

    @Override
    public void schedule(LocalDateTime runTime, Callable<Object> callable) {
        taskQueue.offer(new Task(callable, runTime));
        if (!queueHandler.isAlive()) {
            synchronized (queueHandler) {
                queueHandler.start();
            }
        }
    }
}
