package ru.gritsay;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

public class Task implements Comparable<Task> {

    private static final AtomicLong seq = new AtomicLong(0);

    private final Callable<Object> action;
    private final LocalDateTime time;
    private final long num;

    Task(Callable<Object> action, LocalDateTime time) {
        this.action = action;
        this.time = time;
        this.num = seq.getAndIncrement();
    }

    Callable<Object> getAction() {
        return action;
    }

    LocalDateTime getTime() {
        return time;
    }

    private long getNum() {
        return num;
    }

    @Override
    public int compareTo(Task task) {
        int result = getTime().compareTo(task.getTime());
        return result == 0 ? Long.compare(getNum(), task.getNum()) : result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "action=" + action +
                ", time=" + time +
                ", num=" + num +
                '}';
    }
}
