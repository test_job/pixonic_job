package ru.gritsay;

import java.util.concurrent.PriorityBlockingQueue;

public class JobStarter {


    public static void main(String[] args) {
        PriorityBlockingQueue<Task> taskQueue = new PriorityBlockingQueue<>();
        new SchedulerImpl(taskQueue);
    }
}
