package ru.gritsay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

public class SchedulerQueueHandler extends Thread {

    private final static Logger log = LoggerFactory.getLogger(SchedulerQueueHandler.class);
    private final PriorityBlockingQueue<Task> taskQueue;

    SchedulerQueueHandler(PriorityBlockingQueue<Task> taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public synchronized void start() {
        while (!Thread.currentThread().isInterrupted() && taskQueue.size() > 0) {
            try {
                Task task = taskQueue.take();
                long delay = ChronoUnit.MILLIS.between(LocalDateTime.now(), task.getTime());

                //TODO Возможно здесь необходимо реализовать пул поток для таймеров
                new Timer().schedule(new TimerTask() {
                                         @Override
                                         public void run() {
                                             try {
                                                 task.getAction().call();
                                             } catch (Exception e) {
                                                 log.error("Error schedule task {}", task);
                                             }
                                         }
                                     }
                        , delay < 0 ? 0 : delay);
            } catch (Exception e) {
                log.error("Error with {}", e.getLocalizedMessage());
            }
        }
    }
}
